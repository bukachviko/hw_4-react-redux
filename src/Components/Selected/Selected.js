import React from "react";
import { BsHeart } from "react-icons/bs";
import "./Selected.css";
const Selected = ({countSelectedItems}) => {
    return (
        <>
            <div className="navbar__selected">
                <div>
                    <BsHeart />
                </div>
                {countSelectedItems > 0 && <div className="navbar__selected-count">{countSelectedItems}</div>}
            </div>
        </>
    )
}

export default Selected;