import Button from "../Button/Button";
import "./ProductCard.css";
import {HiStar} from "react-icons/hi2";
import {HiOutlineStar} from "react-icons/hi2";
import {useDispatch, useSelector} from "react-redux";
import {checkCurrentProductIdToCart, toggleSelectedProducts} from "../Slices/productsSlice";
import {openModal} from "../Slices/modalSlice";
import React from "react";

function ProductCard(props) {
    const dispatch = useDispatch();
    const selected = useSelector(state => state.products.selected);

    return (
        <>
            <div>
                <img className="card__image-product" width="150" height="150" src={props.image} alt=""/>
                <h3 className="card__title-product">{props.name}</h3>
                <p className="card__color-product">{props.color}</p>
                <p className="card__price-product"> ${props.cost}</p>
                <p className="card__article-product">{props.article}</p>
                <div className="actions">
                    <Button
                        key={1}
                        btnClass="btn__add-to-cart"
                        backgroundColor={'#833ab4'}
                        text={"Add to cart"}

                        myClick={
                            () => {
                                dispatch(openModal())
                                dispatch(checkCurrentProductIdToCart(props.productId))
                            }
                        }
                    />
                    <div className="actions actions__selected"
                         onClick={() => {
                             dispatch(toggleSelectedProducts({
                                 id: props.productId,
                                 name: props.name,
                                 color: props.color,
                                 cost: props.cost,
                                 image: props.image
                             }))
                         }
                         }>

                        {
                            (selected.filter(item => item.id === props.productId)).length
                                ? <HiStar fill='rgba(131,58,180,1)'/>
                                : <HiOutlineStar/>
                        }
                    </div>
                </div>
            </div>
        </>
    );
}

export default ProductCard;
