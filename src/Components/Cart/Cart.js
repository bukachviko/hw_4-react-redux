import React from "react";
import {FaShoppingCart} from "react-icons/fa";
import "./Cart.css";

const Cart = ({countCartItems}) => {
    return (
        <>
            <div className="navbar__shipping">
                <div>
                    <FaShoppingCart/>
                </div>
                {countCartItems > 0 && <div className="navbar__shipping-count">{countCartItems}</div>}
            </div>
        </>
    )
}

export default Cart;
