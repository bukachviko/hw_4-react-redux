import React, {useState} from "react";
import './App.css';
import Header from "./Components/Header/Header";
import {BrowserRouter as Router, Route, Routes} from "react-router-dom";
import {Homepage} from "./Components/Pages/Homepage/Homepage";
import {Cartpage} from "./Components/Pages/Cartpage/Cartpage";
import {Selectedpage} from "./Components/Pages/Selectedpage/Selectedpage";

function App() {
    return (
        <div className="wrapper">
            <Router>
                <Header/>
                <Routes>
                    <Route index path="/" element={<Homepage/>}/>
                    <Route path="/selected" element={<Selectedpage/>}/>
                    <Route path="/cart" element={<Cartpage/>}/>
                </Routes>
            </Router>
        </div>
    )
}

export default App;
